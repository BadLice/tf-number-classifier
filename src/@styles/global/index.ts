import { css, Theme } from '@emotion/react';

export const globalStyles = (theme: Theme) =>
	css({
		'*': {
			fontFamily: `'Roboto', sans-serif`,
		},
		body: {
			backgroundColor: theme.colors.background.primary,
		},
	});
