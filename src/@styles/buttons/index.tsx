import { css, SerializedStyles } from '@emotion/react';
import styled from '@emotion/styled';

export const ActionButton = styled.button(
	({ theme }): SerializedStyles =>
		css({
			backgroundColor: theme.colors.buttons.primary,
			flex: 1,
			padding: '8px 8px',
			color: theme.colors.text.primary,
			outline: 'none',
			border: `1px solid ${theme.colors.borders.primary}`,
			margin: 3,
			transition: '0.2s background-color',

			'&:hover': {
				background: theme.colors.buttons.hover,
				transition: '0.2s background-color',
			},

			'&:active': {
				background: theme.colors.buttons.active,
				transition: '0.2s background-color',
			},
		})
);
