/** @jsxImportSource @emotion/react */

import { Theme } from '@emotion/react';

declare module '@emotion/react' {
	export interface Theme {
		colors: {
			background: { primary: string; secondary: string };
			text: {
				primary: string;
				secondary: string;
			};
			buttons: {
				primary: string;
				secondary: string;
				hover: string;
				active: string;
			};
			borders: {
				primary: string;
				secondary: string;
			};
		};
	}
}

export const theme: Theme = {
	colors: {
		borders: {
			primary: 'black',
			secondary: '#ddd',
		},
		background: { primary: '#555', secondary: '#777' },
		text: {
			primary: 'orange',
			secondary: 'white',
		},
		buttons: {
			primary: '#484848',
			secondary: '#ddd',
			hover: '#707070',
			active: 'black',
		},
	},
};
