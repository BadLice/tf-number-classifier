import { css, SerializedStyles } from '@emotion/react';
import styled from '@emotion/styled';

export const DrawableCanvas = styled.canvas(
	({ theme }): SerializedStyles =>
		css({
			border: `1px dashed ${theme.colors.borders.secondary}`,
		})
);
