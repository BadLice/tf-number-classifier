import { css, SerializedStyles } from '@emotion/react';
import styled from '@emotion/styled';

export const ColContainer = styled.fieldset(
	({ theme }): SerializedStyles =>
		css({
			flex: 1,
			padding: 10,
			border: `solid 1px ${theme.colors.borders.secondary}`,
			alignItems: 'center',
			justifyContent: 'center',
			textAlign: 'center',
		})
);

export const ColLegend = styled.legend(
	({ theme }): SerializedStyles =>
		css({
			color: theme.colors.text.primary,
		})
);

export const RowContainer = styled.div(
	(): SerializedStyles =>
		css({
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'center',
			textAlign: 'center',
		})
);

export const AppContainer = styled.div(
	(): SerializedStyles =>
		css({
			position: 'absolute',
			top: '50%',
			left: '50%',
			marginRight: '-50%',
			transform: 'translate(-50%, -50%)',
			minWidth: 1000,
		})
);

export const appSection = css({
	minWidth: 500,
	minHeight: 500,
});
