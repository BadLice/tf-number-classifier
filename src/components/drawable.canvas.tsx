import {
	DrawableCanvasHelpers,
	DrawablecanvasProps,
	Position,
} from '@customTypes/drawable.canvas';
import { DrawableCanvas } from '@styles/components/drawable.canvas';
import {
	createRef,
	forwardRef,
	MutableRefObject,
	useEffect,
	useImperativeHandle,
	useState,
} from 'react';
import { NeuralNumberPixel } from '../@customTypes/model';
import { useModel } from '../contexts/model.context';

export default forwardRef<DrawableCanvasHelpers, DrawablecanvasProps>(function Drawablecanvas(
	{ size, lineWeight, color },
	helperRef
) {
	const { drawablescale, nnSize } = useModel();
	let drawableRef = createRef<HTMLCanvasElement>();
	let scaledRef = createRef<HTMLCanvasElement>();
	let [isDown, setIsDown] = useState<boolean>(false);
	let [ctx, setCtx] = useState<CanvasRenderingContext2D>();

	useEffect(() => {
		if ((drawableRef! as MutableRefObject<HTMLCanvasElement>).current) {
			setCtx(
				(
					(drawableRef! as MutableRefObject<HTMLCanvasElement>)
						.current! as HTMLCanvasElement
				).getContext('2d')!
			);
		}
	}, [drawableRef]);

	const getScaledImageData = (saleFactor: number): ImageData => {
		scaledRef.current!.width = size * saleFactor;
		scaledRef.current!.height = size * saleFactor;

		let helper2Ctx = scaledRef.current!.getContext('2d');
		helper2Ctx!.scale(saleFactor, saleFactor);
		helper2Ctx!.drawImage(drawableRef.current!, 0, 0);

		return helper2Ctx!.getImageData(
			0,
			0,
			scaledRef.current!.width,
			scaledRef.current!.height
		);
	};

	const getImageData = () =>
		ctx!.getImageData(0, 0, drawableRef.current!.width, drawableRef.current!.height);
	const clear = () =>
		ctx!.clearRect(0, 0, drawableRef.current!.width, drawableRef.current!.height);

	const toNeuralNumberPixelArray = (): Array<NeuralNumberPixel> => {
		let { data: pixels } = getScaledImageData(1 / drawablescale);

		const isOn = (pixels: Uint8ClampedArray, i: number) =>
			pixels[i * (pixels.length / (nnSize * nnSize))] !== 0 ||
			pixels[i * (pixels.length / (nnSize * nnSize)) + 1] !== 0 ||
			pixels[i * (pixels.length / (nnSize * nnSize)) + 2] !== 0;

		let numShape: Array<NeuralNumberPixel> = Array.from({ length: 28 * 28 }, () => 0);
		return numShape.map((_, i) => (isOn(pixels, i) ? 1 : 0));
	};

	const draw = (
		ctx: CanvasRenderingContext2D,
		x: number,
		y: number,
		lineWeight: number
	): void => {
		ctx.fillStyle = color;
		ctx.beginPath();
		ctx.arc(x, y, lineWeight, 0, Math.PI * 2, true);
		ctx.closePath();
		ctx.fill();
	};

	const onMouseDown = (e: React.MouseEvent<HTMLCanvasElement>): void => {
		setIsDown(true);
		let pos = getMousePos(e);
		draw(ctx!, pos.x, pos.y, lineWeight);
	};

	const onMouseUp = () => {
		setIsDown(false);
	};

	const onMouseMove = (e: React.MouseEvent<HTMLCanvasElement>): void => {
		getMousePos(e);
		if (isDown) {
			let pos = getMousePos(e);
			draw(ctx!, pos.x, pos.y, lineWeight);
		}
	};

	const getMousePos = (e: React.MouseEvent<HTMLCanvasElement>): Position => {
		var rect = (
			drawableRef! as MutableRefObject<HTMLCanvasElement>
		).current!.getBoundingClientRect();
		let x = e.clientX - rect.left;
		let y = e.clientY - rect.top;
		return { x, y };
	};

	useImperativeHandle(helperRef, () => ({
		getImageData,
		clear,
		getScaledImageData,
		toNeuralNumberPixelArray,
	}));

	return (
		<>
			<DrawableCanvas
				width={size}
				height={size}
				onMouseMove={onMouseMove}
				onMouseUp={onMouseUp}
				onMouseDown={onMouseDown}
				ref={drawableRef}
			></DrawableCanvas>

			<canvas width={0} height={0} ref={scaledRef} style={{ display: 'none' }}></canvas>
		</>
	);
});
