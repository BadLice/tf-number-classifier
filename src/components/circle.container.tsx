/** @jsxImportSource @emotion/react */

import { CircleContainerProps as ConcentricCircleProps } from '@customTypes/circle.container';
import { css, Global, keyframes, useTheme } from '@emotion/react';
import { createRef } from 'react';

function ConcentricCircle({
	children,
	size,
	borderSize,
	animate = false,
	fontSize = 90,
}: ConcentricCircleProps) {
	const theme = useTheme();
	const innerCircleRef = createRef<HTMLDivElement>();
	const outerCircleRef = createRef<HTMLDivElement>();

	const container1 = css({
		position: 'relative',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column',
	});
	const container2 = css({
		position: 'relative',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
	});

	const growhtAnimation = keyframes({
		from: {
			fontSize: 0,
		},
		to: {
			fontSize: fontSize,
		},
	});
	const animateMiddleCircle = css({
		animation: `${growhtAnimation} 0.7s ease-in-out`,
		animationFillMode: 'forwards', //make animation stay at end
	});
	const middleCircle = css({
		position: 'absolute',
		borderRadius: '50%',
		height: size,
		width: size,
		backgroundColor: theme.colors.background.primary,
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		// fontSize: fontSize,
		color: theme.colors.text.primary,
		userSelect: 'none',
	});

	const progressAnimation = keyframes({
		from: {
			'--perc': '0%',
		},
		to: {
			'--perc': '100%',
		},
	});
	const animateSpinner = css({
		animation: `${progressAnimation} 0.7s ease-in-out`,
		animationFillMode: 'forwards', //make animation stay at end
	});
	const idleSpinner = css({
		borderRadius: '50%',
		height: size + borderSize - 6,
		width: size + borderSize - 6,
		background: `conic-gradient(${theme.colors.text.primary} var(--perc), ${theme.colors.background.secondary} var(--perc))`,
	});

	return (
		<div css={container1}>
			<Global
				styles={css`
					@property --perc {
						syntax: '<percentage>';
						initial-value: 0%;
						inherits: false;
					}
				`}
			/>
			<div css={container2}>
				<div
					css={animate ? [middleCircle, animateMiddleCircle] : middleCircle}
					ref={innerCircleRef}
				>
					{children}
				</div>
				<div
					css={animate ? [idleSpinner, animateSpinner] : idleSpinner}
					ref={outerCircleRef}
				></div>
			</div>
		</div>
	);
}

export default ConcentricCircle;
