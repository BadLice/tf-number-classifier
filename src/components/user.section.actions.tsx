/** @jsxImportSource @emotion/react */

import { UserActionsProps } from '@customTypes/user.section.actions';
import { ActionButton } from '@styles/buttons';
import { RowContainer } from '@styles/containers';
import { useModel } from 'contexts/model.context';

function UserActions({ canvasRef, setPrediction }: UserActionsProps) {
	const { model } = useModel();

	const clearCanvas = () => {
		if (canvasRef.current) {
			canvasRef.current.clear();
			setPrediction(undefined);
		}
	};

	const predict = (): void => {
		if (canvasRef.current!) {
			setPrediction(Number(model.predict(canvasRef.current.toNeuralNumberPixelArray())));
		}
	};

	return (
		<RowContainer>
			<ActionButton onClick={clearCanvas}>Clear</ActionButton>
			<ActionButton onClick={predict}>Predict</ActionButton>
		</RowContainer>
	);
}

export default UserActions;
