/** @jsxImportSource @emotion/react */

import { ModelSectionProps } from '@customTypes/model.section';
import { css } from '@emotion/react';
import { appSection, ColContainer, ColLegend } from '../@styles/containers';
import ConcentricCircle from './circle.container';

function ModelSection({ prediction }: ModelSectionProps) {
	const maxSize = 480;
	const borderSize = 30;

	const generateConcentricCircles = (amount: number, text: string | number | undefined) => {
		const generateRecursive = (
			amount: number,
			index: number,
			text: string | number | undefined
		): React.ReactNode => (
			<ConcentricCircle
				size={maxSize - borderSize * (index + 1)}
				borderSize={borderSize}
				animate={prediction === amount - index - 1}
			>
				{++index === amount ? text : generateRecursive(amount, index, text)}
			</ConcentricCircle>
		);

		return generateRecursive(amount, 0, text);
	};

	return (
		<ColContainer css={[appSection, css({ display: 'flex' })]}>
			<ColLegend>Predictions</ColLegend>
			{generateConcentricCircles(10, prediction)}
		</ColContainer>
	);
}

export default ModelSection;
