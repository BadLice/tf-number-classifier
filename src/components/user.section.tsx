/** @jsxImportSource @emotion/react */

import { UserSectionProps } from '@customTypes/user.section';
import { appSection, ColContainer, ColLegend } from '@styles/containers';
import DrawableCanvas from 'components/drawable.canvas';
import UserActions from 'components/user.section.actions';

function UserSection({ canvasRef, nnSize, drawablescale, setPrediction }: UserSectionProps) {
	return (
		<ColContainer css={appSection}>
			<ColLegend>Draw</ColLegend>
			<DrawableCanvas
				size={nnSize * drawablescale}
				ref={canvasRef}
				lineWeight={17}
				color={'white'}
			/>
			<UserActions canvasRef={canvasRef} setPrediction={setPrediction} />
		</ColContainer>
	);
}

export default UserSection;
