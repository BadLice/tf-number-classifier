/** @jsxImportSource @emotion/react */
import { ThemeProvider } from '@emotion/react';
import ReactDOM from 'react-dom/client';
import { theme } from './@styles/theme';
import App from './App';
import { ModelContext, modelContextValue } from './contexts/model.context';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
	<ModelContext.Provider value={modelContextValue}>
		<ThemeProvider theme={theme}>
			<App />
		</ThemeProvider>
	</ModelContext.Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
