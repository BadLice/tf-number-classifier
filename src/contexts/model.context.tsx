import { createContext, useContext } from 'react';
import { Model } from '../tf-nn/model';

export interface ModelContextType {
	nnSize: number;
	drawablescale: number;
	model: Model;
}

export const modelContextValue: ModelContextType = {
	nnSize: 28, //the size of a number in MINST dataset is 28x28
	drawablescale: 15,
	model: new Model(),
};

export const ModelContext = createContext<ModelContextType | undefined>(undefined);

export const useModel = () => {
	const modelCtx = useContext(ModelContext);
	if (!modelCtx)
		throw new Error('No GridItemContext.Provider found when calling useGridItemContext.');
	return modelCtx;
};
