import * as tf from '@tensorflow/tfjs';
import { Rank, Tensor } from '@tensorflow/tfjs';
import { MinstIO, MinstIOType, NeuralNumber, NeuralNumberPixel } from '../@customTypes/model';

export interface Model {
	network: tf.LayersModel;
	labelList: string[];
	isReady: boolean;
	learningrate: number;
	trainingData: MinstIO;
	testingData: MinstIO;
}

export class Model {
	static labelList = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

	constructor() {
		this.isReady = false;
		this.learningrate = 0.0005;
	}

	async setup(onEnd?: Function): Promise<void> {
		this.network = await tf.loadLayersModel('./nn-data/my-model.json');

		if (onEnd) {
			onEnd();
		}
	}

	getReadyForTraining(onEnd?: Function): void {
		if (!this.isReady) {
			if (!this.network) {
				console.log('No model loaded -----> genereting a new one');
				this.network = tf.sequential({
					layers: [
						//layer to recognize patterns in images
						tf.layers.conv2d({
							inputShape: [28, 28, 1], // width x height x color_channel (for rgb is 3, for bw is 1)
							filters: 32, //number of patterns to recognize (similar to units of a dense layer); must be in range(0 - width x height)
							kernelSize: [5, 5], // size of the filters to apply (a filter is a matrix, common values are 3x3, 5x5, 7x7 based on the image size)
							activation: 'relu',
							padding: 'same', //ensure that the output shape is tha same as the input shape adding zero-padding to the borders of the image (must be used for small images and for consecutive conv2d layers)
						}),
						//duplacte the first layer to increase trainable params
						tf.layers.conv2d({
							filters: 32,
							kernelSize: [5, 5],
							activation: 'relu',
							padding: 'same',
						}),
						// layer to downscale an image -> used to recognize more complex patterns further in the layers
						tf.layers.maxPooling2d({
							poolSize: 2, // every 2x2 matrix of pixels will become 1 pixel
							strides: 2, // for every downscaled pixel, shift the pool matrix (2x2 in this case) of 2 positions
						}),
						// layer to prevent overfitting. it adds a calculated number to the calculated loss when the loss is very low -> to the network this loss will become high like if the network mad a wrong prediction
						tf.layers.dropout({
							rate: 0.25, //hyperparameter to tune the value added to the loss; A good value for dropout in a hidden layer is between 0.5 and 0.8 for dense layers, and 0.1/0.2 for convolutional layers. dropout should increase progressively further on the layers
						}),
						tf.layers.conv2d({
							filters: 64,
							kernelSize: [3, 3],
							activation: 'relu',
							padding: 'same',
						}),
						tf.layers.conv2d({
							filters: 64,
							kernelSize: [3, 3],
							activation: 'relu',
							padding: 'same',
						}),
						tf.layers.maxPooling2d({
							poolSize: 2,
							strides: 2,
						}),
						tf.layers.dropout({
							rate: 0.25,
						}),
						//flatten layer: convert a multi dimensional layer (like conv2d) to a 1d layer
						tf.layers.flatten(),
						tf.layers.dense({ units: 256, activation: 'relu' }),
						tf.layers.dropout({
							rate: 0.5,
						}),
						tf.layers.dense({ units: 10, activation: 'softmax' }),
					],
				});
			}
			this.network.summary();
			this.network.compile({
				loss: 'categoricalCrossentropy',
				optimizer: tf.train.adam(this.learningrate),
				metrics: ['accuracy'],
			});
			this.isReady = true;
			console.log('Model compiled -----> can be trained');
		}
		if (onEnd) {
			onEnd();
		}
	}

	async train(): Promise<void> {
		let trainingData = await this.getDataTensors(MinstIOType.TRAINING);
		await this.network.fit(trainingData.inputs, trainingData.outputs, {
			epochs: 5,
			shuffle: true,
			validationSplit: 0.1,
			callbacks: {
				onTrainBegin: () => console.log('start training'),
				onTrainEnd: () => {
					console.log('end training');
					this.test(Model.testLog);
				},
				onEpochEnd: (epoch, logs) => {
					console.log(epoch + 1, logs);
				},
			},
		});
	}

	predict = (pixels: Array<NeuralNumberPixel>): string =>
		tf.tidy(() => {
			let inputs = tf.tensor4d(pixels as number[], [1, 28, 28, 1]);
			let prediction =
				Model.labelList[
					(this.network.predict(inputs) as Tensor<Rank>).argMax(1).dataSync()[0]
				];
			return prediction;
		});

	test = async (
		callBack: (rightPredictionsAmount: number, totalPredictionsAmount: number) => any,
		index?: number
	) => {
		console.log('Test started.....');
		this.getDataTensors(MinstIOType.TESTING).then((data) => {
			tf.tidy(() => {
				let tempData: MinstIO = index
					? {
							inputs: data.inputs.slice(index, 1),
							outputs: data.outputs.slice(index, 1),
					  }
					: {
							inputs: data.inputs.slice(0, data.inputs.shape[0]),
							outputs: data.outputs.slice(0, data.outputs.shape[0]),
					  };
				let predicted = (this.network.predict(tempData.inputs) as Tensor<Rank>)
					.argMax(1)
					.arraySync() as number[];
				let target = tempData.outputs.argMax(1).arraySync() as number[];
				let rightPredictionsAmount = 0;
				predicted.forEach((prediction, i) =>
					prediction === target[i] ? rightPredictionsAmount++ : null
				);
				tempData.inputs.dispose();
				tempData.outputs.dispose();
				console.log('Test ended');
				callBack(rightPredictionsAmount, predicted.length);
			});
		});
	};

	download = async () => {
		await this.network.save('downloads://my-model');
	};

	async getDataTensors(type: MinstIOType): Promise<MinstIO> {
		//if tensors were already generated and saved, just return them
		if (type === MinstIOType.TESTING) {
			if (this.testingData) {
				return this.testingData;
			}
		} else {
			if (this.trainingData) {
				return this.trainingData;
			}
		}

		//tensors were not saved -> generate them
		//retrieve data from json files
		let minstData = await Model.fetchData(
			`./nn-data/${type === MinstIOType.TESTING ? 'testing' : 'training'}.json`
		);

		//generate tensors
		return tf.tidy(() => {
			//creat 2d input tensor
			let inputs2d = tf.tensor2d([
				...minstData.map((neuralNumber) =>
					[...neuralNumber.shape].map((pixel) =>
						pixel > 255 / 2 ? NeuralNumberPixel.ON : NeuralNumberPixel.OFF
					)
				),
			]);
			//convert input tensor from 2d to 4d
			let inputs: Tensor<Rank.R4> = inputs2d.reshape([minstData.length, 28, 28, 1]);
			inputs2d.dispose();

			//rotate images 90 degrees anti-clockwise and mirror themself
			inputs = tf.image.rotateWithOffset(inputs, -Math.PI / 2);
			inputs = tf.reverse4d(inputs, [2, 3]);

			//create tensor containing the indeces of the labels
			let indices = tf.tensor1d(
				[
					...minstData.map((neuralNumber) =>
						Model.labelList.indexOf(neuralNumber.label!)
					),
				],
				'int32'
			);
			//create output layer as a oneHot
			let outputs = tf.oneHot(indices, Model.labelList.length);
			indices.dispose();

			//save generated tensors
			if (type === MinstIOType.TESTING) {
				this.testingData = { inputs, outputs };
			} else {
				this.trainingData = { inputs, outputs };
			}

			return { inputs, outputs };
		});
	}

	static fetchData = async (path: string): Promise<Array<NeuralNumber>> => {
		let fileRead = await fetch(path, {
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
			},
		});
		let dataArray: Array<{
			label: string;
			data: Array<number>;
		}> = await fileRead.json();

		return dataArray.map(({ label, data }) => ({
			label,
			shape: data,
		}));
	};

	static testLog = (rightPredictionsAmount: number, totalPredictionsAmount: number): void =>
		console.table({
			'right predictions': rightPredictionsAmount,
			total: totalPredictionsAmount,
			accuracy: `${(rightPredictionsAmount / totalPredictionsAmount) * 100} %`,
		});
}
