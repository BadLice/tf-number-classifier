/** @jsxImportSource @emotion/react */
import { DrawableCanvasHelpers } from '@customTypes/drawable.canvas';
import { MinstIOType, NeuralNumber, NeuralNumberPixel } from '@customTypes/model';
import * as tf from '@tensorflow/tfjs';
import DrawableCanvas from 'components/drawable.canvas';
import { useModel } from 'contexts/model.context';
import { createRef, useEffect, useState } from 'react';
import { Model } from './model';

function ModelTesting() {
	let { model } = useModel();
	let [testData, setTestdata] = useState<NeuralNumber[]>([]);

	let canvasRef = createRef<DrawableCanvasHelpers>();

	const nnSize = 28; //the size of a number in MINST dataset is 28x28
	const drawablescale = 15;

	useEffect(() => {
		if (!model.isReady) {
			model.setup();
			// setInterval(() => console.log(tf.memory()), 5000);
		}

		if (testData.length === 0) {
			model.getDataTensors(MinstIOType.TESTING).then(({ inputs, outputs }) => {
				tf.tidy(() => {
					let shapes = inputs.slice(0, 1000).arraySync() as number[][][][];
					let target = outputs.argMax(1).arraySync() as number[];

					// console.log('shapes', JSON.stringify(shapes));

					let arr2d: number[][] = [];
					shapes.forEach((shape) => {
						let arr1d: number[] = ([] as number[]).concat(
							...shape.map((xAxis) => xAxis.map((val) => val[0]))
						);
						arr2d.push(arr1d);
					});

					let res: NeuralNumber[] = arr2d.map((shape, i) => ({
						label: Model.labelList[target[i]],
						shape,
					}));
					setTestdata(res);
					console.log('loaded samples');
					// inputs.dispose();
					// outputs.dispose();
				});
			});
		} else {
			testData.forEach(({ label, shape }, i) => drawTest(shape, `canvas-${i}`));
		}
	}, [model, model.isReady, testData, testData.length]);

	const parseCanvas = (): Array<NeuralNumberPixel> => {
		let { data: pixels } = canvasRef.current!.getScaledImageData(1 / drawablescale);

		const isOn = (pixels: Uint8ClampedArray, i: number) =>
			pixels[i * (pixels.length / (nnSize * nnSize))] !== 0 ||
			pixels[i * (pixels.length / (nnSize * nnSize)) + 1] !== 0 ||
			pixels[i * (pixels.length / (nnSize * nnSize)) + 2] !== 0;

		let numShape: Array<NeuralNumberPixel> = Array.from({ length: 28 * 28 }, () => 0);
		return numShape.map((_, i) => (isOn(pixels, i) ? 1 : 0));
	};

	const predict = (): void => {
		console.log('prediction', model.predict(parseCanvas()));
		drawTest(parseCanvas());
	};

	const predictTest = (index: number) => {
		tf.tidy(() => {
			model.getDataTensors(MinstIOType.TESTING).then(({ inputs, outputs }) => {
				inputs = inputs.slice(index, 1);
				console.log(
					'predicting canvas ' + index,
					Model.labelList[
						(model.network.predict(inputs) as tf.Tensor<tf.Rank>)
							.argMax(1)
							.dataSync()[0]
					]
				);
			});
		});

		model.test(Model.testLog, index);
	};

	const drawTest = (px: Array<NeuralNumberPixel>, id?: string) => {
		let canvas = document.getElementById(id ?? 'canvas');
		let ctx = (canvas! as HTMLCanvasElement).getContext('2d');
		ctx!.clearRect(0, 0, nnSize, nnSize);
		ctx!.clearRect(0, 0, nnSize, nnSize);
		px.forEach((v, i) => {
			var x = Math.floor(i % nnSize);
			var y = Math.floor(i / nnSize);
			ctx!.fillStyle = v === NeuralNumberPixel.OFF ? 'white' : `black`;
			ctx!.fillRect(x, y, 1, 1);
		});
	};

	return (
		<>
			<DrawableCanvas
				size={nnSize * drawablescale}
				ref={canvasRef}
				lineWeight={17}
				color={'grey'}
			/>
			<button onClick={predict}>predict</button>
			<button onClick={() => canvasRef.current!.clear()}>clear</button>
			<button
				onClick={async () => {
					await model.network.save('downloads://my-model');
				}}
			>
				download
			</button>
			<button
				onClick={async () => {
					model.test(Model.testLog);
				}}
			>
				test
			</button>
			<button
				onClick={async () => {
					model.getReadyForTraining();
					model.train().then((test) => console.log(test));
				}}
			>
				train
			</button>
			<input
				type='text'
				onChange={(e) => {
					drawTest(JSON.parse(e.target.value), 'canvas-manual');
				}}
			/>
			<canvas id='canvas-manual' width={nnSize} height={nnSize}></canvas>
			<canvas id='canvas' width={nnSize} height={nnSize}></canvas>
			{testData.map((_, i) => (
				<canvas
					id={`canvas-${i}`}
					key={i}
					width={nnSize}
					height={nnSize}
					onClick={() => predictTest(i)}
				></canvas>
			))}{' '}
		</>
	);
}

export default ModelTesting;
