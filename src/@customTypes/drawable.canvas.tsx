import { NeuralNumberPixel } from './model';

export interface DrawablecanvasProps {
	size: number;
	lineWeight: number;
	color: string;
}

export interface Position {
	x: number;
	y: number;
}

export interface DrawableCanvasHelpers {
	getImageData: () => ImageData;
	clear: () => void;
	getScaledImageData: (saleFactor: number) => ImageData;
	toNeuralNumberPixelArray: () => Array<NeuralNumberPixel>;
}
