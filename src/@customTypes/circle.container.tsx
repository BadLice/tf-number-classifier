export interface CircleContainerProps {
	borderSize: number;
	size: number;
	children?: React.ReactNode;
	animate?: boolean;
	fontSize?: number;
}
