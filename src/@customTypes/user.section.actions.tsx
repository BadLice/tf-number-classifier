import { DrawableCanvasHelpers } from './drawable.canvas';

export interface UserActionsProps {
	canvasRef: React.RefObject<DrawableCanvasHelpers>;
	setPrediction: React.Dispatch<React.SetStateAction<number | undefined>>;
}
