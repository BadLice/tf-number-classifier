import { DrawableCanvasHelpers } from './drawable.canvas';

export interface UserSectionProps {
	canvasRef: React.RefObject<DrawableCanvasHelpers>;
	nnSize: number;
	drawablescale: number;
	setPrediction: React.Dispatch<React.SetStateAction<number | undefined>>;
}
