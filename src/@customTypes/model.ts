import { Rank, Tensor } from '@tensorflow/tfjs';

export enum MinstIOType {
	TRAINING = 1,
	TESTING = 0,
}
export interface MinstIO {
	inputs: Tensor<Rank.R4>;
	outputs: Tensor<Rank>;
}

export enum NeuralNumberPixel {
	OFF = 0,
	ON = 1,
}
export interface NeuralNumber {
	label: string | null;
	shape: Array<NeuralNumberPixel>;
}
