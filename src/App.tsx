/** @jsxImportSource @emotion/react */
import { DrawableCanvasHelpers } from '@customTypes/drawable.canvas';
import { css, Global, useTheme } from '@emotion/react';
import { AppContainer, RowContainer } from '@styles/containers';
import { globalStyles } from '@styles/global';
import ModelSection from 'components/model.section';
import UserSection from 'components/user.section';
import { useModel } from 'contexts/model.context';
import { createRef, useEffect, useState } from 'react';
import WebFont from 'webfontloader';

WebFont.load({ google: { families: ['Roboto:300,400,500'] } });

let appContainer = css({
	maxWidth: 1000,
});

function App() {
	const theme = useTheme();
	let { model } = useModel();

	let canvasRef = createRef<DrawableCanvasHelpers>();

	const nnSize = 28; //the size of a hand-drown number in MINST dataset is 28x28
	const drawablescale = 15;

	useEffect(() => {
		if (!model.isReady) {
			model.setup();
		}
	}, [model, model.isReady]);

	const [prediction, setPrediction] = useState<number>();

	return (
		<AppContainer>
			<Global styles={globalStyles(theme)} />
			<RowContainer css={appContainer}>
				<UserSection
					canvasRef={canvasRef}
					nnSize={nnSize}
					drawablescale={drawablescale}
					setPrediction={setPrediction}
				/>
				<ModelSection prediction={prediction} />
			</RowContainer>
		</AppContainer>
	);
}

export default App;
